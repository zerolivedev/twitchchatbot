# Twitch ChatBot

Learning about TwitchChat and bots while stream in www.twitch.com/zerolivedev ;P

## System requirements

- `Docker version 20.10.18 or compatible`
- `Docker Compose version v2.3.3 or compatible`

## How to run the project

Just by using the following script `sh scripts/all/up.sh`, but first of all, you needs to create a `.env` file with the environment variables well filled in `.example_env`

> Once you end of use the project I recommend to you stop it by using `sh scripts/all/down.sh`

## Documentation

- [Twitch Chat & Chatbots](https://dev.twitch.tv/docs/irc)
- [Twitch Chat keep alive messages](https://dev.twitch.tv/docs/irc/#keepalive-messages)
- [Twitch IRC auth token](https://twitchapps.com/tmi/)
