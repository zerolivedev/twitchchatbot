FROM ruby:3.1.2
WORKDIR /opt/chatbot

COPY Gemfile* ./

RUN bundle install

COPY . .

CMD ["ruby", "src/main.rb"]
