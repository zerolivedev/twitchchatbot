require_relative "../../../../../src/twitch/chatbot/commands"
require_relative "../../../../../src/support/moment"

module Twitch
  class Chatbot
    module Builder
      def self.a_command
        return CommandBuilder.new
      end

      class CommandBuilder
        def initialize
          @descriptor = {
            requester: "player_nickname",
            command: ::Twitch::Chatbot::Commands::HELP,
            message: "",
            moment: Moment.now,
            channel: "a_channel"
          }
        end

        def with
          return self
        end

        def and
          return self
        end

        def at_channel(value)
          @descriptor[:channel] = value

          return self
        end

        def requester(value)
          @descriptor[:requester] = value

          return self
        end

        def message(value)
          @descriptor[:message] = value

          return self
        end

        def as_start_duel
          @descriptor[:command] = ::Twitch::Chatbot::Commands::DUEL

          return self
        end

        def as_duel_move
          @descriptor[:command] = duel_move_command

          return self
        end

        def as_play_rpsls
          @descriptor[:command] = ::Twitch::Chatbot::Commands::SHAPE

          return self
        end

        def at_moment(moment)
          @descriptor[:moment] = moment

          return self
        end

        def to_left
          @descriptor[:message] = "left"

          return self
        end

        def lizard
          @descriptor[:message] = "lizard"

          return self
        end

        def build
          return Command.new(**@descriptor)
        end

        private

        def duel_move_command
          duel = ::Twitch::Chatbot::Commands::DUEL
          move = ::Twitch::Chatbot::Commands::MOVE

          return duel + " " + move
        end
      end

      class Command
        def initialize(requester:, command:, message:, moment:, channel:)
          @requester = requester
          @command = command
          @message = message
          @moment = moment
          @channel = channel
        end

        def requester
          return @requester
        end

        def channel
          return @channel
        end

        def moment
          return @moment
        end

        def shape
          return @message
        end

        def duel_movement
          return @message
        end

        def name
          return @message.split("!").last
        end

        def custom_command_text
          return @message.split(":").last
        end

        def custom_command_to_create
          return @message.split(":").first
        end

        def full_message
          return @message
        end

        def is_duel_move?
          return (@command == duel_move_command)
        end

        def is_duel?
          return (@command == ::Twitch::Chatbot::Commands::DUEL)
        end

        def is_requested_by_channel_owner?
          return (requester_channel == @channel)
        end

        private

        def requester_channel
          return "#" + @requester
        end

        def duel_move_command
          duel = ::Twitch::Chatbot::Commands::DUEL
          move = ::Twitch::Chatbot::Commands::MOVE

          return duel + " " + move
        end
      end
    end
  end
end
