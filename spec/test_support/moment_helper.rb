require "time"

module TestSupport
  class Moment
    class << self
      def at_one_minute_ago
        return ::Time.now - 60
      end

      def at_one_minute_in_the_future
        return ::Time.now + 60
      end
    end
  end
end
