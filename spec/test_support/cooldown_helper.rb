require_relative "../../src/twitch/chatbot/actions/shared/cooldown"

module TestSupport
  class Cooldown
    def self.flush
      Twitch::Chatbot::Actions::Shared::Cooldown.reset
    end
  end
end

module Twitch
  class Chatbot
    module Actions
      class Shared
        def self.reset
          Cooldown.reset
        end

        class Cooldown
          def self.reset
            @@cooldowns = {}
          end
        end
      end
    end
  end
end
