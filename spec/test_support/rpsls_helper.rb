require_relative "cooldown_helper"

module TestSupport
  class RPSLS
    def self.flush
      Cooldown.flush
    end
  end
end
