require_relative "../../src/twitch/chatbot/actions/play_to_untouchable"
require_relative "db_client_helper"
require_relative "cooldown_helper"

module TestSupport
  module Untouchable
    def self.flush
      Twitch::Chatbot::Actions::PlayToUntouchable.flush
    end

    module Builder
      def self.as_lose_game
        return UntouchableGameBuilder.new.as_lose
      end

      def self.as_won_game
        return UntouchableGameBuilder.new.as_won
      end

      class UntouchableGameBuilder
        def initialize
          @descriptor = {
            is_lost: false,
            is_won: false,
          }
        end

        def with
          return self
        end

        def as_lose
          @descriptor[:is_lost] = true

          return self
        end

        def as_won
          @descriptor[:is_won] = true

          return self
        end

        def previous_winner(nickname, times_won)
          ::Twitch::Chatbot::Actions::PlayToUntouchable.add_winners(nickname, times_won)

          return self
        end

        def build
          return Game.new(**@descriptor)
        end

        class Game
          def initialize(is_lost:, is_won:)
            @is_lost = is_lost
            @is_won = is_won
          end

          def challenged_by(challenger)
            return Duel.new(challenger, @is_lost, @is_won)
          end

          def is_movement_valid?(_movement)
            return true
          end

          class Duel
            def initialize(challenger, is_lost, is_won)
              @challenger = challenger
              @is_lost = is_lost
              @is_won = is_won
            end

            def duelist_moves_to(_position)

            end

            def duelist
              return @challenger
            end

            def player_left_meters
              return 0
            end

            def is_ongoing?
              return false
            end

            def is_won?
              return @is_won
            end

            def is_lost?
              return @is_lost
            end
          end
        end
      end
    end
  end
end

module Twitch
  class Chatbot
    module Actions
      class PlayToUntouchable
        def self.flush
          @@duel = Duel.new
          TestSupport::Cooldown.flush
          Winners.reset
          PlayedMoments.reset
        end

        def self.add_winners(nickname, times_won)
          times_won.times do
            Winners.register_win_for(nickname)
          end
        end

        class PlayedMoments
          def self.reset
            @@last_moments = {}
          end
        end

        class Winners
          def self.reset
            model = TestSupport::DBClient.retrieve_model(MODEL_NAME)

            model.drop
          end
        end
      end
    end
  end
end
