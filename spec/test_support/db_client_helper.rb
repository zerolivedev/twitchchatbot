require_relative "../../src/twitch/chatbot/actions/play_to_untouchable/winners"
require_relative "../../src/twitch/chatbot/actions/custom_commands/repository"
require_relative "../../src/twitch/chatbot/actions/play_to_rpsls/winners"
require_relative "../../src/twitch/chatbot/channels_to_join"
require_relative "../../src/infrastructure/clients/db"

module TestSupport
  class DBClient < Infrastructure::Clients::DB
    MODELS = [
      Twitch::Chatbot::Actions::PlayToUntouchable::Winners::MODEL_NAME,
      Twitch::Chatbot::Actions::PlayToRPSLS::Winners::MODEL_NAME,
      Twitch::Chatbot::ChannelsToJoin::MODEL_NAME,
      Twitch::Chatbot::Actions::CustomCommands::Repository::MODEL_NAME
    ]

    def self.drop_all_models
      MODELS.each do |model|
        DBClient.retrieve_model(model).drop
      end
    end

    def drop
      @model.drop
    end
  end
end
