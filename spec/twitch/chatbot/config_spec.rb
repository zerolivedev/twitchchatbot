require_relative "../../../src/infrastructure/clients/tcp_socket"
require_relative "../../../src/twitch/chatbot/config"
require_relative "../test_config"

describe "Twitch Bot Config" do
  it "connects to Twitch IRC chat" do
    allow(Infrastructure::Clients::TcpSocket).to receive(:connect)
    twitch_address = TestConfig::TWITCH_IRC_SERVER
    twitch_port = TestConfig::TWITCH_IRC_PORT

    Twitch::Chatbot::Config.twitch_irc_connection

    expect(Infrastructure::Clients::TcpSocket).to have_received(:connect).
      with(twitch_address, twitch_port)
  end
end
