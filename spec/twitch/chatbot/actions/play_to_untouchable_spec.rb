require_relative "../../../../src/twitch/chatbot/actions/play_to_untouchable"
require_relative "../../../test_support/twitch/chatbot/builder/command"
require_relative "../../../test_support/untouchable_helper"
require_relative "../../../test_support/moment_helper"

describe "Play to Untouchable" do
  before :each do
    TestSupport::Untouchable.flush
  end

  it "starts the duel on request" do
    requester = 'PlayerNickname'
    start_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.with.requester(requester).build

    message = Twitch::Chatbot::Actions::PlayToUntouchable.do(start_duel_command)

    expect(message).to eq("@#{requester} has started a duel against me, gl&hf. For move forward you have to mention me followed by !duel !move <position> (left, center, right)")
  end

  it "starts a new duel after play more than one minute ago" do
    start_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.build
    Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel_command).do
    new_requester = 'new_requester'
    one_minute_in_the_future = TestSupport::Moment.at_one_minute_in_the_future
    start_another_duel_command = Twitch::Chatbot::Builder.a_command.
      as_start_duel.with.
      requester(new_requester).
      at_moment(one_minute_in_the_future).
      build

    message = Twitch::Chatbot::Actions::PlayToUntouchable.new(start_another_duel_command).do

    expect(message).to eq("@#{new_requester} has started a duel against me, gl&hf. For move forward you have to mention me followed by !duel !move <position> (left, center, right)")
  end

  it "does not starts a duel if was started by another player in less than one minute" do
    requester = "First player"
    start_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.with.requester(requester).build
    Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel_command).do
    new_requester = 'new_requester'
    start_another_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.with.requester(new_requester).build

    message = Twitch::Chatbot::Actions::PlayToUntouchable.new(start_another_duel_command).do

    expect(message).to eq("Currently I'm in a duel with @#{requester}")
  end

  it "starts a new duel after end previous duel when has passed a minute" do
    play_until_end_a_duel
    one_minute_in_the_future = TestSupport::Moment.at_one_minute_in_the_future
    start_duel = Twitch::Chatbot::Builder.a_command.as_start_duel.with.at_moment(one_minute_in_the_future).build

    message = Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel).do

    expect(message).to eq("@#{start_duel.requester} has started a duel against me, gl&hf. For move forward you have to mention me followed by !duel !move <position> (left, center, right)")
  end

  it "does not allow to play again to player in cooldown" do
    play_until_end_a_duel
    start_duel = Twitch::Chatbot::Builder.a_command.as_start_duel.build

    message = Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel).do

    expect(message).to include("@#{start_duel.requester} just end a game, you have to rest at least a minute before playing again. Timeleft: ")
  end

  describe "says the TOP 5 players" do
    it "on lose against player" do
      requester = "First player"
      start_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.with.requester(requester).build
      lost_game = TestSupport::Untouchable::Builder.as_lose_game.build
      Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel_command, lost_game).do
      move_command = Twitch::Chatbot::Builder.a_command.as_duel_move.to_left.with.requester(requester).build

      message = Twitch::Chatbot::Actions::PlayToUntouchable.new(move_command).do

      expect(message).to eq("@#{move_command.requester} dodged all my shoots and touch me, CONGRATULATIONS! Currently you won 1 time and the TOP 5 winners are: 1️⃣ #{requester} 1 wins")
    end

    it "on win against player" do
      requester = "First player"
      start_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.with.requester(requester).build
      won_game = TestSupport::Untouchable::Builder.as_won_game.with.previous_winner("Somebody", 9).build
      Twitch::Chatbot::Actions::PlayToUntouchable.new(start_duel_command, won_game).do
      move_command = Twitch::Chatbot::Builder.a_command.as_duel_move.to_left.with.requester(requester).build

      message = Twitch::Chatbot::Actions::PlayToUntouchable.new(move_command).do

      expect(message).to eq("@#{move_command.requester} loses because was hit by my shot, LOSER!. 0 meters left to reach me and win. TOP 5 winners are: 1️⃣ Somebody 9 wins")
    end
  end

  def play_until_end_a_duel
    move_in_duel_command = Twitch::Chatbot::Builder.a_command.as_duel_move.to_left.build
    start_another_duel_command = Twitch::Chatbot::Builder.a_command.as_start_duel.build
    Twitch::Chatbot::Actions::PlayToUntouchable.new(start_another_duel_command).do
    begin
      message = Twitch::Chatbot::Actions::PlayToUntouchable.new(move_in_duel_command).do
    end while not message.include?('LOSER') and not message.include?('CONGRATULATIONS')
  end
end
