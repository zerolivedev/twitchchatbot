require_relative "../../../../../src/twitch/chatbot/actions/play_to_untouchable/played_moments"
require_relative "../../../../../src/support/moment"

describe "Played Moments" do
  before :each do
    TestSupport::Untouchable.flush
  end

  it "responds with was passed more than a minute for the first" do
    played_moments = Twitch::Chatbot::Actions::PlayToUntouchable::PlayedMoments.new(Moment.now, a_channel)

    is_was_a_minute_ago = played_moments.was_a_minute_ago?

    expect(is_was_a_minute_ago).to be_truthy
  end

  it "responds with was not passed more than a minute when registers the moment in less than a minute" do
    played_moments = Twitch::Chatbot::Actions::PlayToUntouchable::PlayedMoments.new(Moment.now, a_channel)

    played_moments.register_play

    expect(played_moments.was_a_minute_ago?).to be_falsy
  end

  it "registers moments for different channels" do
    played_moments = Twitch::Chatbot::Actions::PlayToUntouchable::PlayedMoments.new(Moment.now, a_channel)

    played_moments.register_play

    played_moments = Twitch::Chatbot::Actions::PlayToUntouchable::PlayedMoments.new(Moment.now, another_channel)
    expect(played_moments.was_a_minute_ago?).to be_truthy
  end

  def a_channel
    return 'a_channel'
  end

  def another_channel
    return 'another_channel'
  end
end
