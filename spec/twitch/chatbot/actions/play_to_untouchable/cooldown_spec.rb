require_relative "../../../../../src/twitch/chatbot/actions/shared/cooldown"
require_relative "../../../../test_support/cooldown_helper"
require_relative "../../../../../src/support/moment"

describe "Cooldown" do
  before(:each) do
    Twitch::Chatbot::Actions::Shared::Cooldown.reset
  end

  let(:cooldown_in_seconds) { 2 }
  let(:current_moment) { Moment.now }
  let(:current_player) { "player_nickname" }
  let(:current_channel) { "current_channel" }

  it "knows if is not active for a player at channel" do
    cooldown = Twitch::Chatbot::Actions::Shared::Cooldown.new(cooldown_in_seconds, current_moment, current_player, current_channel)

    is_on_cooldown = cooldown.is_active_for_current_player?

    expect(is_on_cooldown).to be_falsy
  end

  it "knows if is active for a player at channel" do
    cooldown = Twitch::Chatbot::Actions::Shared::Cooldown.new(cooldown_in_seconds, current_moment, current_player, current_channel)
    cooldown.register_finish_game_for_current_player
    cooldown.register_finish_game_for_current_player

    is_on_cooldown = cooldown.is_active_for_current_player?

    expect(is_on_cooldown).to be_truthy
  end

  it "knows the time left for a player at channel" do
    cooldown = Twitch::Chatbot::Actions::Shared::Cooldown.new(cooldown_in_seconds, current_moment, current_player, current_channel)
    cooldown.register_finish_game_for_current_player

    timeleft = cooldown.timeleft_for_current_player

    expect(timeleft).to eq(cooldown_in_seconds)
  end

  it "checks by channel" do
    cooldown_for_a_channel = Twitch::Chatbot::Actions::Shared::Cooldown.new(cooldown_in_seconds, current_moment, current_player, "a_channel")
    cooldown_for_a_channel.register_finish_game_for_current_player
    cooldown_for_a_channel.register_finish_game_for_current_player

    cooldown_for_another_channel = Twitch::Chatbot::Actions::Shared::Cooldown.new(cooldown_in_seconds, current_moment, current_player, "another_channel")
    cooldown_for_a_channel.register_finish_game_for_current_player

    expect(cooldown_for_a_channel.is_active_for_current_player?).to be_truthy
    expect(cooldown_for_another_channel.is_active_for_current_player?).to be_falsy
  end
end
