require_relative "../../../../../src/twitch/chatbot/actions/play_to_rpsls/messages"

describe "Play to RPSLS messages" do
  let(:player_nick) { "player" }

  it "builds an message when the player is the winner" do

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "scissors",
      bot_shape: "paper",
      game_result: "Player wins"
    )

    expect(message).to eq("@#{player_nick}'s Scissors cuts Bot's Paper. Player wins!")
  end

  it "builds an message when the bot is the winner" do

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "paper",
      bot_shape: "scissors",
      game_result: "Bot wins"
    )

    expect(message).to eq("Bot's Scissors cuts @#{player_nick}'s Paper. Bot wins!")
  end

  it "builds a draw message" do

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "paper",
      bot_shape: "paper",
      game_result: "Draw"
    )

    expect(message).to eq("There is a tie between the @#{player_nick} and the bot because they both chose Paper.")
  end

  it "changes the verb depending how to the winning shape wons the other shape" do
    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "paper",
      bot_shape: "scissors",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Scissors cuts @#{player_nick}'s Paper. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "rock",
      bot_shape: "paper",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Paper covers @#{player_nick}'s Rock. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "lizard",
      bot_shape: "rock",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Rock crushes @#{player_nick}'s Lizard. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "spock",
      bot_shape: "lizard",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Lizard poisons @#{player_nick}'s Spock. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "scissors",
      bot_shape: "spock",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Spock smashes @#{player_nick}'s Scissors. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "lizard",
      bot_shape: "scissors",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Scissors decapitates @#{player_nick}'s Lizard. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "paper",
      bot_shape: "lizard",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Lizard eats @#{player_nick}'s Paper. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "spock",
      bot_shape: "paper",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Paper disproves @#{player_nick}'s Spock. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "rock",
      bot_shape: "spock",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Spock vaporizes @#{player_nick}'s Rock. Bot wins!")

    message = Twitch::Chatbot::Actions::PlayToRPSLS::Messages.build_for(
      player_nick: player_nick,
      player_shape: "scissors",
      bot_shape: "rock",
      game_result: "Bot wins"
    )
    expect(message).to eq("Bot's Rock crushes @#{player_nick}'s Scissors. Bot wins!")
  end
end
