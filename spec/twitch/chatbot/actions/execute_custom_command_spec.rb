require_relative "../../../../src/twitch/chatbot/actions/execute_custom_command"
require_relative "../../../../src/twitch/chatbot/actions/create_command"
require_relative "../../../test_support/twitch/chatbot/builder/command"
require_relative "../../../test_support/db_client_helper"
require_relative "../../../spy/twitch/chat"

describe "Executes a custom command" do
  before(:each) do
    TestSupport::DBClient.drop_all_models
    @chat_spy = Spy::Twitch::Chat.enable
  end

  it "when is previously created for the channel" do
    owner_nickname = "owner"
    channel_owner = "#" + owner_nickname
    text_command = "youtube_link"
    custom_command = "socials"
    create_custom_command = Twitch::Chatbot::Builder.a_command.with.requester(channel_owner).
      at_channel(channel_owner).and.
      requester(owner_nickname).and.
      message(custom_command + ":" + text_command).
      build
    Twitch::Chatbot::Actions::CreateCommand.do(create_custom_command)
    execute_custom_command = Twitch::Chatbot::Builder.a_command.
      at_channel(channel_owner).and.
      message("!" + custom_command).
      build

    Twitch::Chatbot::Actions::ExecuteCustomCommand.do(execute_custom_command, @chat_spy)

    expect(@chat_spy.last_message).to be_include(text_command)
  end

  it "when is previously created for the channel (2)" do
    owner_nickname = "owner"
    channel_owner = "#" + owner_nickname
    text_command = "youtube_link"
    custom_command = "socials"
    create_custom_command = Twitch::Chatbot::Builder.a_command.with.requester(channel_owner).
      at_channel(channel_owner).and.
      requester(owner_nickname).and.
      message(custom_command + ":" + text_command).
      build
    Twitch::Chatbot::Actions::CreateCommand.do(create_custom_command)
    another_custom_command = "akira"
    another_text = "hi!"
    create_custom_command = Twitch::Chatbot::Builder.a_command.with.requester(channel_owner).
      at_channel(channel_owner).and.
      requester(owner_nickname).and.
      message(another_custom_command + ":" + another_text).
      build
    Twitch::Chatbot::Actions::CreateCommand.do(create_custom_command)
    execute_custom_command = Twitch::Chatbot::Builder.a_command.
      at_channel(channel_owner).and.
      message("!" + another_custom_command).
      build

    Twitch::Chatbot::Actions::ExecuteCustomCommand.do(execute_custom_command, @chat_spy)

    expect(@chat_spy.last_message).to be_include(another_text)
  end

  it "does nothing if it is not created for the channel" do
    custom_command = " !socials"
    channel_owner = "#owner"
    execute_custom_command = Twitch::Chatbot::Builder.a_command.
      at_channel(channel_owner).and.
      message(" !" + custom_command).
      build
    requester = execute_custom_command.requester

    Twitch::Chatbot::Actions::ExecuteCustomCommand.do(execute_custom_command, @chat_spy)

    expect(@chat_spy.last_message).to be_include("#{requester} 🤷‍♂️ Unkown command use !help for know all the commands")
  end
end
