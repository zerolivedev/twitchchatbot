require_relative "../../../../src/twitch/chatbot/actions/play_to_rpsls"
require_relative "../../../test_support/twitch/chatbot/builder/command"
require_relative "../../../test_support/db_client_helper"
require_relative "../../../test_support/moment_helper"
require_relative "../../../test_support/rpsls_helper"

describe "Play to RPSLS" do
  before :each do
    TestSupport::DBClient.drop_all_models
    TestSupport::RPSLS.flush
  end

  it "does not allow to play again to player in cooldown" do
    play_rpsls = Twitch::Chatbot::Builder.a_command.as_play_rpsls.with.lizard.build
    Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls).do
    timeleft = "59"

    message = Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls).do

    expect(message).to eq("@#{play_rpsls.requester} just end a game, you have to rest at least a minute before playing again. Timeleft: #{timeleft}")
  end

  it "allows to play again to player not in cooldown" do
    play_rpsls = Twitch::Chatbot::Builder.a_command.as_play_rpsls.with.lizard.build
    Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls).do
    one_minute_in_the_future = TestSupport::Moment.at_one_minute_in_the_future
    play_rpsls_after_a_minute = Twitch::Chatbot::Builder.a_command.as_play_rpsls.with.lizard.at_moment(one_minute_in_the_future).build

    message = Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls_after_a_minute).do

    expect(message).not_to include("@#{play_rpsls.requester} just end a game, you have to rest at least a minute before playing again. Timeleft:")
    expect(message).to include("Lizard")
  end

  it "shows the top three winners after a game" do
    top_player = "top_player"
    play_to_rpsls_until_win(top_player)
    play_to_rpsls_until_win(top_player)
    one_minute_in_the_future = TestSupport::Moment.at_one_minute_in_the_future
    play_rpsls = Twitch::Chatbot::Builder.a_command.as_play_rpsls.with.lizard.
      at_moment(one_minute_in_the_future).
      build

    message = Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls).do

    expect(message).to include(" Currently you won ")
    expect(message).to include(" time and the 🏆 TOP 3: 1️⃣ #{top_player} 2 wins")
  end

  def play_to_rpsls_until_win(nickname)
    begin
      TestSupport::RPSLS.flush
      play_rpsls = Twitch::Chatbot::Builder.a_command.as_play_rpsls.with.lizard.and.requester(nickname).build

      message = Twitch::Chatbot::Actions::PlayToRPSLS.new(play_rpsls).do
    end while not message.include?("Player wins")
  end
end
