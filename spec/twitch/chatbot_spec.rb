require_relative "../../src/twitch/chatbot/actions/answer_to_help"
require_relative "../test_support/untouchable_helper"
require_relative "../test_support/db_client_helper"
require_relative "../test_support/rpsls_helper"
require_relative "../../src/twitch/chatbot"
require_relative "../spy/irc_server"
require_relative "test_config"

describe "Twitch Chatbot" do
  before :each do
    TestSupport::DBClient.drop_all_models
    irc_server = Spy::IrcServer.prepare
    @chat_spy = irc_server.spy
    @config = TestConfig.new(@chat_spy)
  end

  describe "on connect" do
    before :each do
      Twitch::Chatbot.new(@config)
    end
    let(:host) { @config.host_nickname }
    let(:joined_channel) { "#" + host }
    let(:another_joined_channel) { "#zerobotdevelopment" }

    it "authenticates with its username and password" do
      expect(@chat_spy.anyone_has_provided_username?).to be_truthy
      expect(@chat_spy.anyone_has_provided_password?).to be_truthy
    end

    it "joins to default channels" do
      expect(@chat_spy.anyone_on_channel?(joined_channel)).to be_truthy
    end

    it "greet in the joined channels" do
      expect(@chat_spy.includes_private_message?(help_message)).to be_truthy
    end

    describe "executes a command for" do
      it "respond with help when somebody ask for it" do

        @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !help")

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?(help_message)).to be_truthy
      end

      it "join to channel when is requested" do
        requester = "zeroliv3"

        @chat_spy.send_private_message("#{bot_mention} !join_my_channel", requester)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.anyone_on_channel?("#" + requester)).to be_truthy
      end

      it "join to a channel requested by host" do
        requester = host
        target_channel = "#any_channel"

        @chat_spy.send_private_message("#{bot_mention} !join_channel #{target_channel}", requester)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.anyone_on_channel?(target_channel)).to be_truthy
      end

      it "does not join to a channel requested by anyone" do
        requester = "anyone"
        target_channel = "#any_channel"

        @chat_spy.send_private_message("#{bot_mention} !join_channel #{target_channel}", requester)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.anyone_on_channel?(target_channel)).not_to be_truthy
        expect(@chat_spy.includes_private_message?("PRIVMSG ##{host} :@#{requester} cannot ask me to join a channel that you does not owns")).to be_truthy
      end

      it "leave a channel when the owner request it" do
        requester = "zeroliv3"
        requester_channel = "#" + requester
        @chat_spy.send_private_message("#{bot_mention} !join_my_channel", requester)

        @chat_spy.send_private_message("#{bot_mention} !leave_my_channel", requester, requester_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("PRIVMSG #zeroliv3 :@zeroliv3 if you want me here again, mention me at #{joined_channel} and use !join_my_channel")).to be_truthy
        expect(@chat_spy.anyone_on_channel?("#" + requester)).to be_falsy
      end


      it "leave a channel when host request it" do
        requester = "zeroliv3"
        requester_channel = "#" + requester
        @chat_spy.send_private_message("#{bot_mention} !join_my_channel", requester)

        @chat_spy.send_private_message("#{bot_mention} !leave_channel #{requester_channel}", host, requester_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.anyone_on_channel?("#" + requester)).to be_falsy
      end

      it "creates a custom command by the owner channel for owner channel" do
        custom_command = "socials"
        text_command = "youtube_link"
        owner_channel = joined_channel
        owner = host

        @chat_spy.send_private_message("#{bot_mention} !create_command #{custom_command}:#{text_command}", owner, owner_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("PRIVMSG #{owner_channel} :✅👍 DEMO: #{text_command}")).to be_truthy
      end

      it "does not create a custom command by the no owner channel" do

        @chat_spy.send_private_message("#{bot_mention} !create_command custom_command:text_command", host, another_joined_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("PRIVMSG #{another_joined_channel} :@#{host} 🙅‍♀️ are not able to create commands in this channel")).to be_truthy
      end

      it "respond only in the channel when is mentioned" do

        @chat_spy.send_private_message("#{joined_channel} :!shape invalid")

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("use invalid shape, you only can use: rock, paper, scissors, lizard, spock")).not_to be_truthy
      end

      it "responds on the channel where comes the request" do
        requester = "somenick"
        private_message = " :@#{requester} only can use the following shapes: rock, paper, scissors, lizard, spock. Example: mention me !shape lizard"

        @chat_spy.send_private_message("#{bot_mention} !shape invalid", requester, another_joined_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("PRIVMSG #{joined_channel} " + private_message)).to be_falsy
        expect(@chat_spy.includes_private_message?("PRIVMSG #{another_joined_channel}" + private_message)).to be_truthy
      end

      describe "plays" do
        describe "RPSLS" do
          before :each do
            TestSupport::RPSLS.flush
          end

          it "can play it" do

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !shape rock")

            Twitch::Chatbot.new(@config)
            expect(@chat_spy.includes_private_message?("Rock")).to be_truthy
          end

          it "responds with help when somebody uses an incorrect shape" do
            incorrect_shape = 'ratón'

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !shape " + incorrect_shape)

            Twitch::Chatbot.new(@config)
            expect(@chat_spy.includes_private_message?("#{joined_channel} :@ only can use the following shapes: rock, paper, scissors, lizard, spock. Example: mention me !shape lizard")).to be_truthy
          end
        end

        describe "Untouchable SAO" do
          before :each do
            TestSupport::Untouchable.flush
          end

          it "starts a duel when somebody ask for it and its free" do

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel", challenger)

            Twitch::Chatbot.new(@config)
            expect(@chat_spy.includes_private_message?("PRIVMSG #{joined_channel} :@#{challenger} has started a duel against me, gl&hf. For move forward you have to mention me followed by !duel !move <position> (left, center, right)")).to be_truthy
          end

          it "does not starts a new duel when another one is ongoing" do
            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel", challenger)

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel", "another_challenger")

            Twitch::Chatbot.new(@config)
            expect(@chat_spy.includes_private_message?("PRIVMSG #{joined_channel} :Currently I'm in a duel with @#{challenger}")).to be_truthy
          end

          it "shoots when the challenger do a movement" do
            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel", challenger)

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel !move left", challenger)

            Twitch::Chatbot.new(@config)

            include_win_message = @chat_spy.includes_private_message?("#{joined_channel} :@#{challenger} dodged all my shoots and touch me, CONGRATULATIONS!")
            includes_lose_message = @chat_spy.includes_private_message?("#{joined_channel} :@#{challenger} loses because was hit by my shot, LOSER!. 5 meters left to reach me and win.")
            includes_dodge_message = @chat_spy.includes_private_message?("#{joined_channel} :@#{challenger} advances a meter because I miss the shoot. 5 meters left to win.")
            expect(include_win_message || includes_lose_message || includes_dodge_message).to be_truthy
          end

          it "notifies when duel movement is invalid" do
            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel_start", challenger)

            @chat_spy.send_private_message("#{joined_channel} :#{bot_mention} !duel !move to_the_moon", challenger)

            Twitch::Chatbot.new(@config)
            expect(@chat_spy.includes_private_message?("PRIVMSG #{joined_channel} :@#{challenger} only can move to left, center or right")).to be_truthy
          end
        end
      end
    end

    describe "executes a custom text command" do
      it "after create it" do
        custom_command = "socials"
        text_command = "youtube_link"
        @chat_spy.send_private_message("#{bot_mention} !create_command #{custom_command}:#{text_command}", host, joined_channel)

        @chat_spy.send_private_message("#{bot_mention} !#{custom_command}", host, joined_channel)

        Twitch::Chatbot.new(@config)
        expect(@chat_spy.includes_private_message?("PRIVMSG #{joined_channel} :#{text_command}")).to be_truthy
      end
    end
  end

  def help_message
    return "#{joined_channel} :(BETA) Hey!!! I'm here! You can interact with me by using: ➡️ #{bot_mention} !help ➡️ #{bot_mention} !shape ➡️ #{bot_mention} !duel ➡️ #{bot_mention} !join_my_channel ➡️ #{bot_mention} !leave_my_channel"
  end

  def challenger
    return "another nickname"
  end

  def bot_mention
    return "@#{@config.bot_nick}"
  end
end
