require_relative "../../src/infrastructure/logger"

class TestConfig
  TWITCH_IRC_SERVER = "irc.chat.twitch.tv"
  TWITCH_IRC_PORT = 6667
  BOT_NICKNAME = "test_nickname"
  BOT_PASSWORD = "test_password"

  def initialize(connection)
    connection.register_host(host_nickname)

    @connection = connection
  end

  def twitch_irc_connection
    return @connection
  end

  def twitch_irc_server
    return TWITCH_IRC_SERVER
  end

  def twitch_irc_port
    return TWITCH_IRC_PORT
  end

  def bot_nick
    return BOT_NICKNAME
  end

  def bot_password
    return BOT_PASSWORD
  end

  def logger
    return TestConfig::TestLogger
  end

  def host_nickname
    return ENV['HOST_NICKNAME']
  end

  private

  class TestLogger < Infrastructure::Logger
    class << self
      private

      def output
        no_output = "/dev/null"

        return no_output
      end
    end
  end
end
