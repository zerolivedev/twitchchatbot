require_relative "../../../../src/infrastructure/clients/tcp_socket/request"
require_relative "../../../../src/twitch/clients/chat/request"

describe Twitch::Clients::Chat::Request do
  it "retrieves a shape" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !shape rock"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    shape = request.shape

    expect(shape).to eq("rock")
  end

  it "retrieves the duel movement" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !duel !move left"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    duel_movement = request.duel_movement

    expect(duel_movement).to eq("left")
  end

  it "retrieves the requester" do
    line = ":requester!requester@requester.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !shape rock"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    requester = request.requester

    expect(requester).to eq("requester")
  end

  it "knows has a mention for a nick" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !shape rock"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    is_mentioned = request.is_mentioned?("zerobotdevelopment")

    expect(is_mentioned).to be_truthy
  end

  it "knows if is a shape request" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !shape rock"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    is_shape = request.is_shape?

    expect(is_shape).to be_truthy
  end

  it "knows if is a help request" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !help"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    is_help = request.is_help?

    expect(is_help).to be_truthy
  end

  it "knows if is a duel request" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !duel"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    is_duel = request.is_duel?

    expect(is_duel).to be_truthy
  end

  it "knows if is a duel move request" do
    line = ":zerolivedev!zerolivedev@zerolivedev.tmi.twitch.tv PRIVMSG #zerolivedev :@zerobotdevelopment !duel !move left"
    tcp_request = Infrastructure::Clients::TcpSocket::Request.from(line)
    request = Twitch::Clients::Chat::Request.from(tcp_request, host_nickname, bot_nickname)

    is_duel_move = request.is_duel_move?

    expect(is_duel_move).to be_truthy
  end

  def host_nickname
    return "host_nickname"
  end

  def bot_nickname
    return "bot_nickname"
  end
end
