require_relative "../../src/games/untouchable_sao"

describe "Untouchable SAO" do
  it "validates movements position" do
    expect(UntouchableSAO.is_movement_valid?("left")).to be_truthy
    expect(UntouchableSAO.is_movement_valid?("center")).to be_truthy
    expect(UntouchableSAO.is_movement_valid?("right")).to be_truthy
    expect(UntouchableSAO.is_movement_valid?("invalid_movement")).to be_falsy
  end

  it "starts the game when a player duels to the bot" do
    shoots = []

    game = UntouchableSAO.new(player_name, shoots)

    expect(game.duelist).to eq(player_name)
  end

  it "is ongoing when the dues is started" do
    shoots = []
    game = UntouchableSAO.new(player_name, shoots)

    is_ongoing = game.is_ongoing?

    expect(is_ongoing).to be_truthy
  end

  context "after start" do
    it "knows the meters left to player wins" do
      game = UntouchableSAO.new(player_name, ["left", "left", "left"])

      current_player_left_meters = game.player_left_meters

      expect(current_player_left_meters).to eq(2)
    end

    it "stills ongoing until the bot shoots to the duelist" do
      game = UntouchableSAO.new(player_name, ["left", "left", "left"])

      game.duelist_moves_to("center")

      expect(game.is_ongoing?).to be_truthy
    end

    it "wins when the bot shoots to the duelist" do
      duelist_movement = "center"
      positions_to_shoot = [duelist_movement]
      game = UntouchableSAO.new(player_name, positions_to_shoot)

      game.duelist_moves_to(duelist_movement)

      expect(game.is_won?).to be_truthy
    end

    it "loses when the bot has no ammo" do
      game = UntouchableSAO.new(player_name, ["left"])

      game.duelist_moves_to("center")

      expect(game.is_lost?).to be_truthy
    end

    context "at the last moment" do
      it "shoots twice" do
        game = UntouchableSAO.new(player_name, ["left", "left", "left"])

        game.duelist_moves_to("center")
        game.duelist_moves_to("center")

        expect(game.is_lost?).to be_truthy
      end
    end
  end

  private

  def player_name
    return "Some Player"
  end
end
