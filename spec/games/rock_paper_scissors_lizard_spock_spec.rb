require_relative "../../src/games/rock_paper_scissors_lizard_spock"

describe "RockPaperScissorsLizardSpock" do
  it "retrieves random shape" do
    shapes = Games::RockPaperScissorsLizardSpock::SHAPES

    random_shape = Games::RockPaperScissorsLizardSpock.random_shape

    expect(shapes.include?(random_shape)).to be_truthy
  end

  it "knows all the shapes" do
    shapes = Games::RockPaperScissorsLizardSpock::SHAPES

    expect(shapes[0]).to eq("rock")
    expect(shapes[1]).to eq("paper")
    expect(shapes[2]).to eq("scissors")
    expect(shapes[3]).to eq("lizard")
    expect(shapes[4]).to eq("spock")
  end

  context "evaluates" do
    it "that Scissors cuts Paper" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "scissors",
        bot_shape: "paper"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Paper covers Rock" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "rock",
        bot_shape: "paper"
      )

      expect(winner).to eq("Bot wins")
    end

    it "that Rock crushes Lizard" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "rock",
        bot_shape: "lizard"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Lizard poisons Spock" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "lizard",
        bot_shape: "spock"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Spock smashes Scissors" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "spock",
        bot_shape: "scissors"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Scissors decapitates Lizard" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "scissors",
        bot_shape: "lizard"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Lizard eats paper" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "lizard",
        bot_shape: "paper"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Paper disproves Spock" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "paper",
        bot_shape: "spock"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Spock vaporizes Rock" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "spock",
        bot_shape: "rock"
      )

      expect(winner).to eq("Player wins")
    end

    it "that Rock crushes Scissors" do

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: "rock",
        bot_shape: "scissors"
      )

      expect(winner).to eq("Player wins")
    end

    it "that draw when two objects are the same" do
      some_object = "rock"

      winner = Games::RockPaperScissorsLizardSpock.evaluates(
        player_shape: some_object,
        bot_shape: some_object
      )

      expect(winner).to eq("Draw")
    end

    it "raises an invalid shape error" do
      some_object = "rock"

      expect {
        winner = Games::RockPaperScissorsLizardSpock.evaluates(
          player_shape: 'invalid shape',
          bot_shape: some_object
        )
      }.to raise_error(Games::RockPaperScissorsLizardSpock::InvalidShapeError)
    end
  end
end
