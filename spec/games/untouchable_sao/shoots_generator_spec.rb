require_relative "../../../src/games/untouchable_sao/shoots_generator"

describe UntouchableSAO::ShootsGenerator do
  it "gives a list with a number of shoots positions" do
    shoots_quantity = 7

    shoots = UntouchableSAO::ShootsGenerator.generate(shoots_quantity)

    expect(shoots.size).to eq(shoots_quantity)
  end
end
