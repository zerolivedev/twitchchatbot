require_relative "irc_server/request"

module Spy
  class IrcServer
    def self.prepare
      return IrcServer.new
    end

    def initialize
      @spy = Spy.new
    end

    def spy
      return @spy
    end

    class Spy
      def initialize
        @address = nil
        @port = nil
        @channels = []
        @private_messages = []
        @server_messages = []
        @logged_username = false
        @logged_password = false

        @host_channel = "#zerolivedev"
      end

      def connect(address, port)
        @address = address
        @port = port

        self
      end

      def register_host(host_nickname)
        @host_channel = "#" + host_nickname
      end

      def puts(message)
        @logged_username = message.include?("NICK ") unless @logged_username
        @logged_password = message.include?("PASS ") unless @logged_password

        if message.include?("JOIN #")
          channel = message.split("JOIN ").last

          @channels.push(channel)
        end

        if message.include?("PART #")
          channel = message.split("PART ").last

          @channels.delete(channel)
        end

        if message.include?("PRIVMSG #")
          private_message = message.split("PRIVMSG ").last

          @private_messages.push(message)
        else
          @server_messages.push(message)
        end
      end

      def send_private_message(private_message, sender="", channel=@host_channel)
        @private_messages.push(":#{sender}!#{sender}@#{sender}.tmi.twitch.tv PRIVMSG #{channel} :" + private_message)
      end

      def send_server_message(message)
        puts(message)
      end

      def each(&block)
        @private_messages.each do |message|
          request = Request.from(message)

          block.call(request)
        end
      end

      def last_private_message
        return @private_messages.last
      end

      def current_server
        return @address
      end

      def is_there_a_pong?
        return @server_messages.any? { |message| message.include?("PONG") }
      end

      def includes_private_message?(private_message)
        has_message = @private_messages.any? do |message|
          message.include?(private_message)
        end

        return has_message
      end

      def anyone_on_channel?(channel)
        return @channels.include?(channel)
      end

      def anyone_connected_to_address?(address)
        return (@address == address)
      end

      def anyone_connected_to_port?(port)
        return (@port == port)
      end

      def anyone_has_provided_username?
        return @logged_username
      end

      def anyone_has_provided_password?
        return @logged_password
      end
    end
    private_constant :Spy
  end
end
