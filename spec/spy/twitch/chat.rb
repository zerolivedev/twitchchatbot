
module Spy
  module Twitch
    class Chat
      def self.enable
        return Chat.new
      end

      def initialize
        @last_message = nil
      end

      def send_message_at(channel, message)
        @last_message = message
      end

      def last_message
        return @last_message
      end
    end
  end
end
