
module Games
  class RockPaperScissorsLizardSpock
    class InvalidShapeError < StandardError
      def message
        return "only can use the following shapes: rock, paper, scissors, lizard, spock. Example: mention me !shape lizard"
      end
    end
  end
end
