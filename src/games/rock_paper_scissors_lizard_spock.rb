require_relative "rock_paper_scissors_lizard_spock/invalid_shape_error"

module Games
  class RockPaperScissorsLizardSpock
    PLAYER_WIN_MESSAGE = "Player wins"
    BOT_WIN_MESSAGE = "Bot wins"
    ROCK = "rock"
    PAPER = "paper"
    SCISSORS = "scissors"
    LIZARD = "lizard"
    SPOCK = "spock"
    SHAPES = [ROCK, PAPER, SCISSORS, LIZARD, SPOCK]
    class << self
      WINNING_PLAYS = {
        SCISSORS => [PAPER, LIZARD],
        PAPER => [ROCK, SPOCK],
        ROCK => [LIZARD, SCISSORS],
        LIZARD => [SPOCK, PAPER],
        SPOCK => [SCISSORS, ROCK]
      }

      def random_shape
        SHAPES.sample
      end

      def evaluates(player_shape:, bot_shape:)
        check_shape!(player_shape)
        return "Draw" if player_shape == bot_shape
        shape_wins = WINNING_PLAYS.fetch(player_shape)

        if shape_wins.include?(bot_shape)
          return PLAYER_WIN_MESSAGE
        else
          return BOT_WIN_MESSAGE
        end
      end

      private

      def check_shape!(player_shape)
        raise InvalidShapeError.new unless SHAPES.include?(player_shape)
      end
    end
  end
end
