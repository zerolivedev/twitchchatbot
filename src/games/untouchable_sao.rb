require_relative "untouchable_sao/shoots_generator"
require_relative "untouchable_sao/shoots"

class UntouchableSAO
  ONGOING = "ongoing"
  WINS = "wins"
  LOSES = "loses"

  def self.is_movement_valid?(position)
    return ShootsGenerator::POSITIONS.include?(position)
  end

  def self.challenged_by(player_name)
    shoots = ShootsGenerator.generate(7)

    return UntouchableSAO.new(player_name, shoots)
  end

  def initialize(player_name, raw_shoots)
    @duelist = player_name
    @shoots = Shoots.with(raw_shoots)

    @status = ONGOING
  end

  def duelist_moves_to(duelist_position)
    is_dulist_shooted = (duelist_position == shoot)

    if is_dulist_shooted
      wins
    else
      if is_last_movement?
        is_dulist_shooted = (duelist_position == shoot)

        wins if is_dulist_shooted
      end

      loses if has_no_ammo?
    end
  end

  def player_left_meters
    return @shoots.movement_shoots_left
  end

  def duelist
    return @duelist
  end

  def is_ongoing?
    return @status == ONGOING
  end

  def is_won?
    return @status == WINS
  end

  def is_lost?
    return @status == LOSES
  end

  private

  def shoot
    shoot = @shoots.extract

    return shoot
  end

  def wins
    @status = WINS
  end

  def loses
    @status = LOSES
  end

  def is_last_movement?
    return @shoots.is_the_last?
  end

  def has_no_ammo?
    return @shoots.is_empty?
  end
end
