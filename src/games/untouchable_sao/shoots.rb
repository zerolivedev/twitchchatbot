
class UntouchableSAO
  class Shoots
    LAST_MOVEMENT_EXTRA_SHOT = 1
    def self.with(raw_shoots)
      return Shoots.new(raw_shoots)
    end

    def initialize(raw_shoots)
      @shoots = raw_shoots
    end

    def extract
      return @shoots.shift
    end

    def movement_shoots_left
      shoots_left = @shoots.size - LAST_MOVEMENT_EXTRA_SHOT

      return shoots_left
    end

    def is_the_last?
      return @shoots.size == LAST_MOVEMENT_EXTRA_SHOT
    end

    def is_empty?
      return @shoots.empty?
    end
  end
end
