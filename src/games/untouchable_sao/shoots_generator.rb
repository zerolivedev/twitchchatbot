
class UntouchableSAO
  class ShootsGenerator
    POSITIONS = ["right", "center", "left"]

    def self.generate(quantity)
      shoots = Array.new

      quantity.times do
        position = POSITIONS.sample

        shoots.push(position)
      end

      return shoots
    end
  end
end
