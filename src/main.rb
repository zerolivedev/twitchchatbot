require_relative "infrastructure/logger"
require_relative "twitch/chatbot"
require_relative "config"

begin
  Infrastructure::Logger.info("Starting chatbot")

  Twitch::Chatbot.connect
rescue => error
  Infrastructure::Logger.error(error)
end
