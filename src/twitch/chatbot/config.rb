require_relative "../../infrastructure/clients/tcp_socket"
require_relative "../../infrastructure/logger"

module Twitch
  class Chatbot
    module Config
      class << self
        def twitch_irc_connection
          connection = tcp_client.connect(twitch_irc_server, twitch_irc_port)

          return connection
        end

        def twitch_irc_server
          return "irc.chat.twitch.tv"
        end

        def twitch_irc_port
          return 6667
        end

        def bot_nick
          return ENV["NICKNAME"]
        end

        def bot_password
          return ENV["PASSWORD"]
        end

        def logger
          return Infrastructure::Logger
        end

        def host_nickname
          return ENV["HOST_NICKNAME"]
        end

        def untouchable_cooldown
          return ENV["UNTOUCHABLE_COOLDOWN"].to_i
        end

        def rpsls_cooldown
          return ENV["RPSLS_COOLDOWN"].to_i
        end

        private

        def tcp_client
          return Infrastructure::Clients::TcpSocket
        end
      end
    end
  end
end
