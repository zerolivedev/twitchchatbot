require_relative "../../infrastructure/clients/db"
require_relative "../chatbot/config"

module Twitch
  class Chatbot
    class ChannelsToJoin
      MODEL_NAME = :channels_to_join

      class << self
        def find_all
          return [host_channel] + requested_channels_to_join
        end

        def add(channel)
          model.insert_one("channel" => channel)
        end

        def remove(channel)
          model.delete_one_where("channel" => channel)
        end

        private

        def requested_channels_to_join
          raw_channels_to_join = model.find_all
          channels_to_join = raw_channels_to_join.map do |document|
            document["channel"]
          end

          return channels_to_join
        end

        def model
          return Infrastructure::Clients::DB.retrieve_model(MODEL_NAME)
        end

        def host_channel
          return "#" + Config.host_nickname
        end
      end
    end
  end
end
