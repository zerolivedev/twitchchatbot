require_relative "../channels_to_join"
require_relative "answer_to_help"

module Twitch
  class Chatbot
    module Actions
      class JoinToChannelByOwner
        def self.do(command, chat)
          return JoinToChannelByOwner.new(command, chat).do()
        end

        def initialize(command, chat)
          @command = command
          @chat = chat
        end

        def do
          ChannelsToJoin.add(requester_channel)
          @chat.join_channel(requester_channel)

          return message
        end

        private

        def message
          return "@#{@command.requester} I'm on my way"
        end

        def requester_channel
          return @command.requester_channel
        end
      end
    end
  end
end
