require_relative "../channels_to_join"
require_relative "answer_to_help"

module Twitch
  class Chatbot
    module Actions
      class JoinToChannelByHost
        def self.do(command, chat)
          return JoinToChannelByHost.new(command, chat).do()
        end

        def initialize(command, chat)
          @command = command
          @chat = chat
        end

        def do
          return not_host_message unless @command.is_from_host?

          ChannelsToJoin.add(channel_to_join)
          @chat.join_channel(channel_to_join)

          return message
        end

        private

        def message
          return "@#{@command.requester} I'm on my way"
        end

        def not_host_message
          request_mention = "@#{@command.requester}"

          return "#{request_mention} cannot ask me to join a channel that you does not owns"
        end

        def channel_to_join
          return @command.channel_to_join
        end
      end
    end
  end
end
