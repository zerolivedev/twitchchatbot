require_relative "../../../games/rock_paper_scissors_lizard_spock"
require_relative "play_to_rpsls/cooldown"
require_relative "play_to_rpsls/messages"
require_relative "play_to_rpsls/winners"

module Twitch
  class Chatbot
    module Actions
      class PlayToRPSLS
        def self.do(command)
          action = PlayToRPSLS.new(command)
          return action.do
        end

        def initialize(command)
          @bot_shape = Games::RockPaperScissorsLizardSpock.random_shape
          @player_nick = command.requester
          @player_shape = command.shape.downcase
          @channel = command.channel

          @cooldown = Cooldown.with(command)
        end

        def do
          return cooldown_message_for_challenger if @cooldown.is_active_for_current_player?

          play_result = Games::RockPaperScissorsLizardSpock.evaluates(
            player_shape: @player_shape,
            bot_shape: @bot_shape
          )

          register_player_win if is_player_winner?(play_result)
          @cooldown.register_finish_game_for_current_player
          return message_for(play_result)
        rescue Games::RockPaperScissorsLizardSpock::InvalidShapeError => error
          return "@#{@player_nick} " + error.message
        end

        private

        def message_for(result)
          message = Messages.build_for(
            player_nick: @player_nick,
            player_shape: @player_shape,
            bot_shape: @bot_shape,
            game_result: result
          )

          return message + top_three_winners_message
        end

        def register_player_win
          Winners.register_win_for(@player_nick, @channel)
        end

        def top_three_winners_message
          winners_list = Winners.ordered_top_three(@channel)
          winners = winners_list.join(" ")

          return " Currently you won #{requester_times_won} time and the 🏆 TOP 3: " + winners
        end

        def requester_times_won
          return Winners.retrieve_wins_count_for(@player_nick, @channel)
        end

        def cooldown_message_for_challenger
          timeleft = @cooldown.timeleft_for_current_player

          return "@#{@player_nick} just end a game, you have to rest at least a minute before playing again. Timeleft: #{timeleft}"
        end

        def is_player_winner?(result)
          win_player_message = Games::RockPaperScissorsLizardSpock::PLAYER_WIN_MESSAGE

          return (result.include?(win_player_message))
        end
      end
    end
  end
end
