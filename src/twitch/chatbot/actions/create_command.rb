require_relative "custom_commands/repository"

module Twitch
  class Chatbot
    module Actions
      class CreateCommand
        COMMAND_CREATE_SUCCESSFULLY = "✅👍 DEMO: "

        class << self
          def do(command)
            return CreateCommand.new(command).do
          end
        end

        def initialize(command)
          @command = command
        end

        def do
          return not_allowed_message unless @command.is_requested_by_channel_owner?

          CustomCommands::Repository.create(@command.channel, @command.custom_command_to_create, @command.custom_command_text)

          return COMMAND_CREATE_SUCCESSFULLY + @command.custom_command_text
        end

        private

        def not_allowed_message
          return "#{requester_mention} 🙅‍♀️ are not able to create commands in this channel"
        end

        def requester_mention
          return "@#{@command.requester}"
        end
      end
    end
  end
end
