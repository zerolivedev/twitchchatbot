require_relative "../shared/cooldown"
require_relative "../../config"

module Twitch
  class Chatbot
    module Actions
      class PlayToRPSLS
        class Cooldown
          class << self
            def with(command)
              return Shared::Cooldown.new(cooldown, command.moment, command.requester, command.channel)
            end

            private

            def cooldown
              return Config.rpsls_cooldown
            end
          end
        end
      end
    end
  end
end
