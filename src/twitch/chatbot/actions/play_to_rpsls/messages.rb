require_relative "../../../../games/rock_paper_scissors_lizard_spock"

module Twitch
  class Chatbot
    module Actions
      class PlayToRPSLS
        class Messages
          class << self
            def build_for(player_nick:, player_shape:, bot_shape:, game_result:)
              return draw_text_for(player_nick, player_shape) if is_game_draw?(game_result)

              if is_bot_the_winner_in?(game_result)
                winner_part = bot_text_with(bot_shape)
                verb = winner_verb_for(bot_shape, player_shape)
                loser_part = player_text_with(player_nick, player_shape)
              else
                winner_part = player_text_with(player_nick, player_shape)
                verb = winner_verb_for(player_shape, bot_shape)
                loser_part = bot_text_with(bot_shape)
              end

              return "#{winner_part} #{verb} #{loser_part}. #{game_result}!"
            end

            private

            def draw_text_for(player_nick, shape)
              return "There is a tie between the @#{player_nick} and the bot because they both chose #{shape.capitalize}."
            end

            def bot_text_with(shape)
              return "Bot's #{shape.capitalize}"
            end

            def player_text_with(nick, shape)
              return "@#{nick}'s #{shape.capitalize}"
            end

            def winner_verb_for(winner_shape, loser_shape)
              verbs = {
                game::PAPER => {
                  game::ROCK => "covers",
                  game::SPOCK => "disproves"
                },
                game::ROCK => {
                  game::LIZARD => "crushes",
                  game::SCISSORS => "crushes",
                },
                game::LIZARD => {
                  game::SPOCK => "poisons",
                  game::PAPER => "eats"
                },
                game::SPOCK => {
                  game::SCISSORS => "smashes",
                  game::ROCK => "vaporizes"
                },
                game::SCISSORS => {
                  game::LIZARD => "decapitates",
                  game::PAPER => "cuts"
                }
              }

              winner_shape_verbs = verbs.fetch(winner_shape)
              win_verb = winner_shape_verbs.fetch(loser_shape)

              return win_verb
            end

            def game
              return Games::RockPaperScissorsLizardSpock
            end

            def is_game_draw?(game_result)
              return game_result == "Draw"
            end

            def is_bot_the_winner_in?(game_result)
              return game_result == "Bot wins"
            end
          end
        end
      end
    end
  end
end
