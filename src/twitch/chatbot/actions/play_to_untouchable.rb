require_relative "play_to_untouchable/played_moments"
require_relative "../../../games/untouchable_sao"
require_relative "play_to_untouchable/cooldown"
require_relative "play_to_untouchable/winners"
require_relative "play_to_untouchable/duel"

module Twitch
  class Chatbot
    module Actions
      class PlayToUntouchable
        class << self
          def do(command)
            return PlayToUntouchable.new(command).do
          end
        end

        def initialize(command, game=UntouchableSAO)
          @command = command
          @duel = Duel.use(game)
          @played_moment = PlayedMoments.new(command.moment, command.channel)
          @cooldown = Cooldown.with(command)
        end

        def do
          response_message = ""

          if not @duel.started? or @played_moment.was_a_minute_ago?
            response_message = start_duel(@command.requester)
          else
            response_message = play_duel
          end

          @played_moment.register_play
          return response_message
        end

        private

        def start_duel(challenger)
          return cooldown_message_for_challenger if @cooldown.is_active_for_current_player?
          @duel.start_against(challenger)

          return "@#{@duel.duelist} has started a duel against me, gl&hf. For move forward you have to mention me followed by !duel !move <position> (left, center, right)"
        end

        def play_duel
          response_message = ""

          if @command.is_duel_move?
            response_message = move_in_duel
          elsif @command.is_duel?
            response_message = "Currently I'm in a duel with @#{@duel.duelist}"
          end

          return response_message
        end

        def move_in_duel
          return "@#{@duel.duelist} only can move to left, center or right" unless @duel.is_movement_valid?(duelist_movement)
          @duel.duelist_moves_to(duelist_movement)
          response_message = ""

          if @duel.is_ongoing?
            response_message = "@#{@duel.duelist} advances a meter because I miss the shoot. #{player_left_meters} meters left to win."
          elsif @duel.is_won?
            response_message = "@#{@duel.duelist} loses because was hit by my shot, LOSER!. #{player_left_meters} meters left to reach me and win. " + top_five_players
            reset_duel
          elsif @duel.is_lost?
            register_winner_player
            response_message = "@#{@duel.duelist} dodged all my shoots and touch me, CONGRATULATIONS! Currently you won #{requester_times_won} time and the " + top_five_players
            reset_duel
          end

          return response_message
        end

        def reset_duel
          @cooldown.register_finish_game_for_current_player
          @duel.restart
        end

        def requester_times_won
          return Winners.retrieve_wins_count_for(@duel.duelist)
        end

        def register_winner_player
          Winners.register_win_for(@duel.duelist)
        end

        def top_five_players
          ordered_top_five = Winners.ordered_top_five

          return "TOP 5 winners are: " + ordered_top_five.join(" ")
        end

        def cooldown_message_for_challenger
          challenger = @command.requester
          timeleft = @cooldown.timeleft_for_current_player

          return "@#{challenger} just end a game, you have to rest at least a minute before playing again. Timeleft: #{timeleft}"
        end

        def duelist_movement
          return @command.duel_movement
        end

        def player_left_meters
          return @duel.player_left_meters
        end
      end
    end
  end
end
