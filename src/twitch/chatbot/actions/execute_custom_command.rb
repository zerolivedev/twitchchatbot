require_relative "custom_commands/repository"

module Twitch
  class Chatbot
    module Actions
      class ExecuteCustomCommand
        class << self
          def do(command, chat)
            channel_custom_commands = retrieve_custom_commands_for(command.channel)
            if channel_custom_commands.none? { |custom_command| command.full_message.include?(custom_command) }
              chat.send_message_at(command.channel, "#{command.requester} 🤷‍♂️ Unkown command use !help for know all the commands")

              return
            end

            channel_commands = raw_commands(command.channel).to_a
            custom_command = channel_commands.find { |custom_command| custom_command["command"] == command.name }
            chat.send_message_at(command.channel, custom_command["text"])
          end

          private

          def retrieve_custom_commands_for(channel)
            raw_commands = raw_commands(channel)

            commands = raw_commands.to_a.map do |raw_command|
              raw_command["command"]
            end

            return commands
          end

          def raw_commands(channel)
            return CustomCommands::Repository.retrieve_by_channel(channel)
          end
        end
      end
    end
  end
end
