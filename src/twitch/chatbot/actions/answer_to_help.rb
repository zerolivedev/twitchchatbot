
module Twitch
  class Chatbot
    module Actions
      class AnswerToHelp
        class << self
          def do(bot_nickname)
            return message_for(bot_nickname)
          end

          private

          def message_for(bot_nickname)
            bot_mention = "@#{bot_nickname}"

            return "(BETA) Hey!!! I'm here! You can interact with me by using: ➡️ #{bot_mention} !help ➡️ #{bot_mention} !shape ➡️ #{bot_mention} !duel ➡️ #{bot_mention} !join_my_channel ➡️ #{bot_mention} !leave_my_channel"
          end
        end
      end
    end
  end
end
