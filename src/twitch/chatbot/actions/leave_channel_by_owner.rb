require_relative "../channels_to_join"

module Twitch
  class Chatbot
    module Actions
      class LeaveChannelByOwner
        def self.do(command, chat)
          return LeaveChannelByOwner.new(command, chat).do()
        end

        def initialize(command, chat)
          @command = command
          @chat = chat
        end

        def do
          @chat.leave_channel(@command.requester_channel)
          ChannelsToJoin.remove(@command.requester_channel)

          return message
        end

        private

        def message
          return "@#{@command.requester} if you want me here again, mention me at ##{@command.host_nickname} and use !join_my_channel"
        end
      end
    end
  end
end
