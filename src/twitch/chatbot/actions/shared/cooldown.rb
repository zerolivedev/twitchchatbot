require_relative "../../../../support/moment"

module Twitch
  class Chatbot
    module Actions
      class Shared
        class Cooldown
          @@cooldowns = {}

          def initialize(cooldown, moment_at, current_player, current_channel)
            @cooldown = cooldown

            @current_channel = current_channel
            @current_player = current_player
            @current_moment = moment_at.to_i
          end

          def register_finish_game_for_current_player
            initialize_cooldowns_for_current_channel unless has_registered_any_cooldown_for_current_channel?

            @@cooldowns[@current_channel][@current_player] = @current_moment
          end

          def timeleft_for_current_player
            return (current_player_cooldown - @current_moment)
          end

          def is_active_for_current_player?
            return (current_player_cooldown > @current_moment)
          end

          private

          def current_player_cooldown
            initialize_cooldowns_for_current_channel unless has_registered_any_cooldown_for_current_channel?

            player_last_finish = @@cooldowns[@current_channel][@current_player] || Moment.long_time_ago

            return (player_last_finish + @cooldown).to_i
          end

          def initialize_cooldowns_for_current_channel
            return @@cooldowns[@current_channel] = {}
          end

          def has_registered_any_cooldown_for_current_channel?
            return (not @@cooldowns[@current_channel].nil?)
          end
        end
      end
    end
  end
end
