require_relative "../channels_to_join"

module Twitch
  class Chatbot
    module Actions
      class LeaveChannelByHost
        NO_MESSAGE = ""

        def self.do(command, chat)
          return LeaveChannelByHost.new(command, chat).do()
        end

        def initialize(command, chat)
          @command = command
          @chat = chat
        end

        def do
          if @command.is_from_host?
            ChannelsToJoin.remove(@command.requester_channel)
            @chat.leave_channel(@command.channel_to_leave)
          end

          return NO_MESSAGE
        end
      end
    end
  end
end
