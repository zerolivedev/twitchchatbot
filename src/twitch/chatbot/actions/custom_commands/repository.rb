require_relative "../../../../infrastructure/clients/db"

module Twitch
  class Chatbot
    module Actions
      module CustomCommands
        class Repository
          MODEL_NAME = :custom_commands

          class << self
            def create(channel, command, text)
              model.insert_one(
                "channel" => channel,
                "command" => command,
                "text" => text
              )
            end

            def retrieve_by_channel(channel)
              commands = model.find_by("channel" => channel)

              return commands
            end

            private

            def model
              return Infrastructure::Clients::DB.retrieve_model(MODEL_NAME)
            end
          end
        end
      end
    end
  end
end
