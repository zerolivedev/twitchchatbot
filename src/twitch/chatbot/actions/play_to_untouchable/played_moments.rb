require_relative "../../../../support/moment"

module Twitch
  class Chatbot
    module Actions
      class PlayToUntouchable
        class PlayedMoments
          @@last_moments = {}

          def initialize(moment, channel)
            @last_play_moment = moment
            @channel = channel
          end

          def was_a_minute_ago?
            one_minute = 60

            return (last_moment + one_minute) < @last_play_moment
          end

          def register_play
            @@last_moments[@channel] = @last_play_moment
          end

          private

          def last_moment
            last_moment = @@last_moments[@channel]
            return Moment.long_time_ago unless last_moment

            last_moment
          end
        end
      end
    end
  end
end
