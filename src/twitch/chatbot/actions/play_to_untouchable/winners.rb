require_relative "../../../../infrastructure/clients/db"

module Twitch
  class Chatbot
    module Actions
      class PlayToUntouchable
        class Winners
          MODEL_NAME = :untouchable_winners
          NO_TIMES_WON = 0

          class << self
            def retrieve_wins_count_for(nickname)
              winner = winners.find_one_where({ "nickname" => nickname })
              return NO_TIMES_WON unless winner

              return winner["times_won"]
            end

            def register_win_for(nickname)
              times_won = retrieve_wins_count_for(nickname)
              if times_won.zero?
                winners.insert_one({ "nickname" => nickname, "times_won" => 1 })
              else
                winners.update_one_where({ "nickname" => nickname }, { "times_won" => times_won + 1 })
              end
            end

            def ordered_top_five
              top_five_message = []

              top_five_winners.each_with_index do |data, index|
                position = index + 1
                position_emoji = translate_to_emoji(position)
                player_nick = data.first
                wins = data.last
                top_five_message.push("#{position_emoji} #{player_nick} #{wins} wins")
              end

              return top_five_message
            end

            private

            def translate_to_emoji(position)
              emojis = {
                1 => "1️⃣",
                2 => "2️⃣",
                3 => "3️⃣",
                4 => "4️⃣",
                5 => "5️⃣"
              }

              emoji = emojis.fetch(position)
              return emoji
            end

            def top_five_winners
              top_five = ordered_winners.last(5)

              return top_five
            end

            def ordered_winners
              ordered_by_wins = all_winners.sort_by { |_nickname, wins| wins }

              return ordered_by_wins.reverse
            end

            def all_winners
              all_winners = {}

              winners.find_all.each do |winner|
                nickname = winner["nickname"]
                times_won = winner["times_won"]

                all_winners[nickname] = times_won
              end

              return all_winners
            end

            def winners
              return Infrastructure::Clients::DB.retrieve_model(MODEL_NAME)
            end
          end
        end
      end
    end
  end
end
