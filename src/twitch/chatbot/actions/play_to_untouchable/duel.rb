module Twitch
  class Chatbot
    module Actions
      class PlayToUntouchable
        class Duel
          @@duel = nil

          def self.use(game)
            duel = Duel.new

            duel.use(game)

            return duel
          end

          def initialize
            @game = nil
          end

          def use(game)
            @game = game
          end

          def start_against(duelist)
            @@duel = @game.challenged_by(duelist)
          end

          def duelist_moves_to(position)
            @@duel.duelist_moves_to(position)
          end

          def duelist
            return @@duel.duelist
          end

          def player_left_meters
            return @@duel.player_left_meters
          end

          def is_ongoing?
            return @@duel.is_ongoing?
          end

          def restart
            @@duel = nil
          end

          def started?
            return (not @@duel.nil?)
          end

          def is_won?
            return @@duel.is_won?
          end

          def is_lost?
            return @@duel.is_lost?
          end

          def is_movement_valid?(movement)
            return @game.is_movement_valid?(movement)
          end
        end
      end
    end
  end
end
