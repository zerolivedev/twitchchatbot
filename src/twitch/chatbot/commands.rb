
module Twitch
  class Chatbot
    class Commands
      SHAPE = "!shape"
      HELP = "!help"
      DUEL = "!duel"
      MOVE = "!move"
      DUEL_MOVE = DUEL + " " + MOVE
      JOIN_MY_CHANNEL = "!join_my_channel"
      LEAVE_MY_CHANNEL = "!leave_my_channel"
      LEAVE_CHANNEL = "!leave_channel"
      JOIN_CHANNEL = "!join_channel"
      CREATE_COMMAND = "!create_command"
    end
  end
end
