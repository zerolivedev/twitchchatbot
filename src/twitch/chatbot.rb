require_relative "chatbot/actions/join_to_channel_by_owner"
require_relative "chatbot/actions/join_to_channel_by_host"
require_relative "chatbot/actions/execute_custom_command"
require_relative "chatbot/actions/leave_channel_by_owner"
require_relative "chatbot/actions/leave_channel_by_host"
require_relative "chatbot/actions/play_to_untouchable"
require_relative "chatbot/actions/answer_to_help"
require_relative "chatbot/actions/create_command"
require_relative "chatbot/actions/play_to_rpsls"
require_relative "chatbot/channels_to_join"
require_relative "chatbot/config"
require_relative "clients/chat"

module Twitch
  class Chatbot
    def self.connect
      return Chatbot.new(Config)
    end

    def initialize(config)
      @bot_nickname = config.bot_nick

      @chat = Clients::Chat.connect_to_twitch_with(config)
      join_chat_channels
      keep_listen_requests_in_chat
    end

    private

    def keep_listen_requests_in_chat
      @chat.each do |request|
        if request.is_mentioned?(@bot_nickname)
          if request.is_shape?
            message = Actions::PlayToRPSLS.do(request)

            @chat.send_message_at(request.channel, message)
          elsif request.is_create_command?
            message = Actions::CreateCommand.do(request)

            @chat.send_message_at(request.channel, message)
          elsif request.is_help?
            message = Actions::AnswerToHelp.do(@bot_nickname)

            @chat.send_message_at(request.channel, message)
          elsif request.is_duel?
            message = Actions::PlayToUntouchable.do(request)

            @chat.send_message_at(request.channel, message)
          elsif request.is_join_my_channel?
            message = Actions::JoinToChannelByOwner.do(request, @chat)
            @chat.send_message_at(request.channel, message)

            message = Actions::AnswerToHelp.do(@bot_nickname)
            @chat.send_message_at(request.channel, message)
          elsif request.is_join_channel?
            message = Actions::JoinToChannelByHost.do(request, @chat)
            @chat.send_message_at(request.channel, message)

            message = Actions::AnswerToHelp.do(@bot_nickname)
            @chat.send_message_at(request.channel_to_join, message)
          elsif request.is_leave_my_channel?
            message = Actions::LeaveChannelByOwner.do(request, @chat)

            @chat.send_message_at(request.channel, message)
          elsif request.is_leave_channel?
            message = Actions::LeaveChannelByHost.do(request, @chat)

            @chat.send_message_at(request.channel, message)
          else
            Actions::ExecuteCustomCommand.do(request, @chat)
          end
        end
      end
    end

    def join_chat_channels
      all_channels_to_join = ChannelsToJoin.find_all

      all_channels_to_join.each do |channel|
        @chat.join_channel(channel)

        message = Actions::AnswerToHelp.do(@bot_nickname)
        @chat.send_message_at(channel, message)
      end
    end
  end
end
