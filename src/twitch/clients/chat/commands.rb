
module Twitch
  module Clients
    class Chat
      module Commands
        PASSWORD = "PASS"

        class << self
          def password(value)
            return PASSWORD + " #{value}"
          end

          def nick(value)
            return "NICK #{value}"
          end

          def join_channel(channel)
            return "JOIN #{channel}"
          end

          def leave_channel(channel)
            return "PART #{channel}"
          end

          def send_private_message(channel, message)
            return "PRIVMSG #{channel} :#{message}"
          end

          def pong
            return "PONG"
          end
        end
      end
    end
  end
end
