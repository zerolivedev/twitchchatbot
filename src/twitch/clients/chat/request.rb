require_relative "../../../support/moment"
require_relative "../../chatbot/commands"

module Twitch
  module Clients
    class Chat
      class Request
        CHANNEL_ELEMENT = "#"
        REQUESTER_ELEMENT = "!"

        def self.from(tcp_request, host_nickname, bot_nickname)
          return Request.new(tcp_request, host_nickname, bot_nickname)
        end

        def initialize(tcp_request, host_nickname, bot_nickname)
          @host_nickname = host_nickname
          @bot_nickname = bot_nickname
          @tcp_request = tcp_request

          @moment = Moment.now
        end

        def host_nickname
          return @host_nickname
        end

        def requester
          dirty_nickname = @tcp_request.split(REQUESTER_ELEMENT).first
          nickname = dirty_nickname[1..-1]

          return nickname
        end

        def requester_channel
          return "#" + requester
        end

        def full_message
          bot_mention = "@" + @bot_nickname
          message = message_for(bot_mention)

          return message
        end

        def shape
          shape_message = message_for(command::SHAPE)
          shape = shape_message.split(" ").first

          return shape
        end

        def duel_movement
          movement = message_for(command::DUEL_MOVE)

          return movement
        end

        def channel_to_leave
          channel = message_for(command::LEAVE_CHANNEL)

          return channel
        end

        def channel_to_join
          channel = message_for(command::JOIN_CHANNEL)

          return channel
        end

        def name
          command = @tcp_request.split("!").last

          return command
        end

        def custom_command_to_create
          create_command_message = message_for(command::CREATE_COMMAND)
          custom_command = create_command_message.split(":").first

          return custom_command
        end

        def custom_command_text
          raw_custom_command = @tcp_request.split(" " + custom_command_to_create + ":")
          raw_custom_command.shift
          command_text = raw_custom_command.join(":")

          return command_text
        end

        def moment
          return @moment
        end

        def channel
          splitted_request = @tcp_request.split(" ")

          channel = splitted_request.find do |word|
            word.start_with?(CHANNEL_ELEMENT)
          end

          return channel
        end

        def is_requested_by_channel_owner?
          return (requester_channel == channel)
        end

        def is_from_host?
          return (requester == @host_nickname)
        end

        def is_mentioned?(nickname)
          return is_tcp_request_including?(mention_to(nickname))
        end

        def is_join_channel?
          return is_tcp_request_including?(command::JOIN_CHANNEL)
        end

        def is_shape?
          return is_tcp_request_including?(command::SHAPE)
        end

        def is_help?
          return is_tcp_request_including?(command::HELP)
        end

        def is_duel?
          return is_tcp_request_including?(command::DUEL)
        end

        def is_duel_move?
          is_move = is_tcp_request_including?(command::MOVE)

          return (is_duel? and is_move)
        end

        def is_ping?
          ping_message = "PING"

          return is_tcp_request_including?(ping_message)
        end

        def is_join_my_channel?
          return is_tcp_request_including?(command::JOIN_MY_CHANNEL)
        end

        def is_leave_my_channel?
          return is_tcp_request_including?(command::LEAVE_MY_CHANNEL)
        end

        def is_leave_channel?
          return is_tcp_request_including?(command::LEAVE_CHANNEL)
        end

        def is_create_command?
          return is_tcp_request_including?(command::CREATE_COMMAND)
        end

        private

        def message_for(bot_command)
          complete_message = @tcp_request.split(" " + bot_command + " ")
          message = complete_message.last

          return message
        end

        def mention_to(nickname)
          return "@" + nickname
        end

        def command
          return Chatbot::Commands
        end

        def is_tcp_request_including?(value)
          return @tcp_request.include?(value)
        end
      end
    end
  end
end
