require_relative "chat/commands"
require_relative "chat/request"

module Twitch
  module Clients
    class Chat
      def self.connect_to_twitch_with(config)
        return Chat.new(config)
      end

      def initialize(config)
        @logger = config.logger
        @host_nickname = config.host_nickname
        @bot_nick = config.bot_nick

        @connection = connect_with(config)
        do_login_with(config)
      end

      def join_channel(channel)
        @connection.puts Commands.join_channel(channel)

        logger.info("[BOT][JOIN_CHANNEL] #{channel}")
      end

      def leave_channel(channel)
        @connection.puts Commands.leave_channel(channel)

        logger.info("[BOT][LEAVE_CHANNEL] #{channel}")
      end

      def each(&block)
        logger.info("[BOT][WAITING REQUESTS]")

        @connection.each do |tcp_request|
          logger.info("[REQUEST]: #{tcp_request}")
          request = Request.from(tcp_request, @host_nickname, @bot_nick)

          answer_with_pong if request.is_ping?

          block.call(request)
        end
      end

      def send_message_at(channel, message)
        @connection.puts Commands.send_private_message(channel, message)

        logger.info("[BOT][SENDS] #{channel}: #{message}")
      end

      private

      def connect_with(config)
        connection = config.twitch_irc_connection
        logger.info("[BOT][CONNECTED] Server: #{connection.current_server}")

        return connection
      end

      def do_login_with(config)
        bot_nick = config.bot_nick

        @connection.puts Commands.password(config.bot_password)
        @connection.puts Commands.nick(bot_nick)

        logger.info("[BOT][LOGGED IN] Nickname: #{bot_nick}")
      end

      def answer_with_pong
        @connection.puts Commands.pong

        logger.info("[BOT][PONG]")
      end

      def logger
        return @logger
      end
    end
  end
end
