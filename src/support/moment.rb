require 'date'

class Moment
  class << self
    def now
      return Time.now
    end

    def long_time_ago
      return Date.new.to_time
    end
  end
end
