require "logger"

module Infrastructure
  class Logger
    class << self
      @@logger = nil

      def info(message)
        logger.info(message)
      end

      def error(message)
        logger.error(message)
      end

      private

      def logger
        @@logger = ::Logger.new(output) if @@logger.nil?

        return @@logger
      end

      def output
        docker_container_output = "/proc/1/fd/1"
        output = STDOUT

        output = docker_container_output if ENV["CURRENT_ENVIRONMENT"] == "development"

        return output
      end
    end
  end
end
