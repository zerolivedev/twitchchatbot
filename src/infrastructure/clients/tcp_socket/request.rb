require_relative "../../../twitch/chatbot/commands"

module Infrastructure
  module Clients
    class TcpSocket
      class Request
        def self.from(line)
          return new(line)
        end

        def initialize(line)
          @line = line.chomp
        end

        def split(value)
          return @line.split(value)
        end

        def include?(substring)
          return @line.include?(substring)
        end

        def to_s
          return @line
        end
      end
    end
  end
end
