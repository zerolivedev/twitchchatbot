require_relative "db/config"
require "mongo"

module Infrastructure
  module Clients
    class DB
      def self.retrieve_model(model_name)
        return new(model_name)
      end

      def initialize(model_name)
        client = Mongo::Client.new(mongodb_url)

        @model = client[model_name]
      end

      def find_all
        condition_for_retrieve_all = {}

        return find_by(condition_for_retrieve_all)
      end

      def find_by(condition)
        return @model.find(condition)
      end

      def find_one_where(condition)
        return find_by(condition).first
      end

      def insert_one(values)
        @model.insert_one(values)
      end

      def update_one_where(condition, new_values)
        @model.find_one_and_update(condition, { "$set" => new_values})
      end

      def delete_one_where(condition)
        find_by(condition).delete_one
      end

      private

      def mongodb_url
        return Config.mongodb_url
      end
    end
  end
end
