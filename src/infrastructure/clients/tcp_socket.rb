require_relative "tcp_socket/request"
require "socket"

module Infrastructure
  module Clients
    class TcpSocket
      def self.connect(address, port)
        return new(address, port)
      end

      def initialize(address, port)
        @address = address
        @socket = TCPSocket.new(address, port)
      end

      def puts(message)
        @socket.puts(message)
      end

      def each(&block)
        @socket.each_line do |line|
          request = Request.from(line)

          block.call(request)
        end
      end

      def current_server
        return @address
      end
    end
  end
end
