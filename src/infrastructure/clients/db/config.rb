
module Infrastructure
  module Clients
    class DB
      class Config
        class << self
          def mongodb_url
            return ENV['MONGODB_URL']
          end
        end
      end
    end
  end
end
